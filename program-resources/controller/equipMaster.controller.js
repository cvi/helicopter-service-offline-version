function getEquisWithMat() {
	var json = [
	    {'Equnr':'9000040', 'Matnr':'1200-1009429-AA'},
	    {'Equnr':'9000041', 'Matnr':'1200-1014765-AA'},
	    {'Equnr':'9000042', 'Matnr':'1200-1009458-AA'},
	    {'Equnr':'9000050', 'Matnr':'1200-1014850-AA'},
	    {'Equnr':'9000051', 'Matnr':'1200-1002487-AB'},
	    {'Equnr':'9000052', 'Matnr':'1200-1014493-AA'},
	    {'Equnr':'9000053', 'Matnr':'1200-1014495-AA'},
	    {'Equnr':'9000054', 'Matnr':'1200-1009458-AA'}];

	var object = obj.data.d.Equis.results;
	for(var i = 0; i < object.length; i++) {
		object[i].iconFlag = "";
		for(var j = 0; j < json.length; j++) {
			if(object[i].Matnr == json[j].Matnr) {
				object[i].iconFlag = "sap-icon://pushpin-on";
				break;
			}
		}
	}
	return object;
}

//function setEquis() {
//	var json = [
//	    	    {'Equnr':'9000039', 'HequiShtxt':'Helicopter Complete'},
//	    	    {'Equnr':'10000001', 'HequiShtxt':'Fuel'},
//	    	    {'Equnr':'9000051', 'HequiShtxt':'Fuel', 'Matnr':'1200-1002487-AB'},
//	    	    {'Equnr':'9000052', 'HequiShtxt':'Storage', 'Matnr':'1200-1014493-AA'},
//	    	    {'Equnr':'9000053', 'HequiShtxt':'Distribution', 'Matnr':'1200-1014495-AA'},
//	    	    {'Equnr':'9000054', 'HequiShtxt':'Indication', 'Matnr':'1200-1009458-AA'},
//	    	    {'Equnr':'9000040', 'HequiShtxt':'Fuel level quantity indication (ct tank)', 'Matnr':'1200-1009429-AA'},
//	    	    {'Equnr':'9000000', 'HequiShtxt':'Main Tube Side Tank'},
//	    	    {'Equnr':'9000001', 'HequiShtxt':'Flange'},
//	    	    {'Equnr':'9000002', 'HequiShtxt':'Housing Cover'},
//	    	    {'Equnr':'9000003', 'HequiShtxt':'Seal'},
//	    	    {'Equnr':'9000004', 'HequiShtxt':'Main Housing'},
//	    	    {'Equnr':'9000005', 'HequiShtxt':'Inlay'},
//	    	    {'Equnr':'9000042', 'HequiShtxt':'Fuel level quantity indication (si tank)', 'Matnr':'1200-1009458-AA'},
//	    	    {'Equnr':'9000041', 'HequiShtxt':'Sensor connector Front', 'Matnr':'1200-1014765-AA'},
//	    	    {'Equnr':'9000050', 'HequiShtxt':'Pressure Vessel interfaces', 'Matnr':'1200-1014850-AA'}];
//
//	obj.data.d.Equis.results = json;
//}

// Define controller object - includes object functions
sap.ui.controller("program-resources.controller.equipMaster", {

	/*
	 * function for filling equipmentlist in masterpage
	 */
	equipFillList: function() {
//		setEquis();
		var equipments = obj.data.d.Equis;
		var view = this.getView();
		var oModel = new sap.ui.model.json.JSONModel();
		obj.data.d.Equis.results = getEquisWithMat();

		// set data to created model and bind it to the list
		oModel.setData(equipments);
		obj.eqList = view.byId("equipList").setModel(oModel);

		// create listitems and define shown entries
		var listItems =  new sap.m.StandardListItem( {
			title: "{Equnr}",
			description: "{HequiShtxt}",
			icon: "{iconFlag}",
			press: [this.onEquipSelect, this]
		});

		listItems.addStyleClass('equipListElement');

		// define path to list in json object
		obj.eqList.bindItems("/results", listItems);
	},

	/*
	 * function for catching clickevent of listitems
	 */
	onEquipSelect: function(oEvent) {
		// set create button visible
		sap.ui.getCore().byId("createButton").setVisible(true);
		obj.selPath = oEvent.oSource._aSelectedPaths[0];
		obj.equnr = oEvent.oSource._oSelectedItem.mProperties.title;
		obj.eqShtxt = oEvent.oSource._oSelectedItem.mProperties.description;

		//set situation to equip click
		obj.position = "equip";
		crazyArchitectWorld.showMaterial(obj.data.d);
	},

	/*
	 * function for switching detail view
	 */
	createButtonPress: function(oEvent) {
		// get controller instance
 		sap.ui.getCore().byId("equipDetails").getController().oFillForm();
 		// go to next view
		oSplitApp.toDetail("equipDetails");
		sap.ui.getCore().byId("__page1-navButton").setVisible(false);
	},

	/*
	 * function for searching with searchfield
	 */
	onSearch: function(oEvent) {
		console.log("searchField: search for: " + oEvent.getParameter("query"));
		var oFilterShtxt = new sap.ui.model.Filter("HequiShtxt", sap.ui.model.FilterOperator.StartsWith, oEvent.getParameter("query"));
		var oListBinding = obj.eqList.getBinding("items");
		oListBinding.filter([oFilterShtxt]);
	},

	/*
	 * function for livechange of searchfield
	 */
	onLiveChange: function(oEvent) {
		console.log("searchField: liveChange for: "	+ oEvent.getParameter("newValue"));
		var oFilterShtxt = new sap.ui.model.Filter("HequiShtxt", sap.ui.model.FilterOperator.StartsWith, oEvent.getParameter("newValue"));
		var oListBinding = obj.eqList.getBinding("items");
		oListBinding.filter([oFilterShtxt]);
	},

	/**
	* Called when a controller is instantiated and its View controls (if available) are already created.
	* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
	* @memberOf mvc.MVC
	*/
	onInit: function() {
		// delegate before show event
		this.getView().addDelegate({
			onBeforeShow: function(evt) {}
		});
	}
});