function handleImageClick(oEvent) {
	alert("image clicked");
}

/*
 * function for getting object position
 */
function getObjectPosition(substrings) {
	var number = null;
	for(var i = 0; i < substrings.length; i++) {
		var tmp = parseInt(substrings[i]);
		if(isNaN(tmp)) {
			continue;
		} else {
			number = tmp;
			break;
		}
	}
	return number;
}

/**
 * Converts time in format for showing in timepicker
 * @param time -- time string
 * @returns {String} -- formated time string
 */
function convertTime(time) {
	return time.substring(0, 2) + ":" + time.substring(3, time.length-1);
}

/**
 * Converts date in format for showing in datepicker
 * @param oArray -- array with date substrings split by '/'
 * @returns {String} -- formated date string
 */
function convertDate(oArray) {
	for(var i = 0; i < oArray.length; i++) {
		if(oArray[i] == "") {
			continue;
		} else {
			var tmp = oArray[i].split("Date(");
			var oDate = new Date(parseInt(tmp[1].substring(0, tmp[1].length-1)));
			return oDate.getDate() + "." + (oDate.getMonth()+1) + "." + oDate.getFullYear();
		}
	}
}

sap.ui.controller("program-resources.controller.notifDetails", {

	/*
	 * Function for filling formelements for the notification
	 */
	oFillForm: function(json) {
		// get object position from path
		var position = getObjectPosition(obj.selPath.split("/"));
		this.byId("notifFunclocField").setValue(json.results[position].Tplnr);
		this.byId("notifEquipField").setValue(json.results[position].Equnr);
		this.byId("notifEquipName").setValue(json.results[position].Eqname);
		this.byId("notifDmgField").setValue("This is a longtext");
		this.byId("notifShtxtField").setValue(json.results[position].Qmtxt);
		this.byId("notifDatePicker").setValue(json.results[position].Erdat);
		this.byId("notifTimePicker").setValue(json.results[position].Erzeit);
		this.byId("notifPrioField").setSelectedKey(json.results[position].Priok);
		this.byId("notifPrioField").setEnabled(false);
	},

	/*
	 * function for canceling notification overview and for back-navigation
	 */
	oCancelNotifOverview: function() {
		oSplitApp.backDetail();
		oSplitApp.backMaster();
		crazyArchitectWorld.removeModel(obj.data.d);
		document.getElementById('initDetails--initPage-title').innerHTML = obj.tplnr;
		sap.ui.getCore().byId("__page2-navButton").setVisible(true);
	},

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf mvc.MVC
*/
	onInit: function() {
        this.getView().addDelegate({
        	onAfterShow: function(evt) {
        		$("#application").css({ 'background' : '' });
        		$("#application-BG").css({ 'opacity': '1' });
//        		oSplitApp.showMaster();
        	}
        });
	}
});