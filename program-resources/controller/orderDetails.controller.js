/*
 * function for getting object position
 */
function getOrderObjectPosition(substrings) {
	var number = null;
	for(var i = 0; i < substrings.length; i++) {
		var tmp = parseInt(substrings[i]);
		if(isNaN(tmp)) {
			continue;
		} else {
			number = tmp;
			break;
		}
	}
	return number;
}

/*
 * sets background of the documents tab transparent
 */
function oDocButtonClicked() {
	$("#bodyID").css({ 'background' : 'transparent' });
	$("#content").css({ "background" : "transparent" });
	$("#application").css({ "background" : "transparent" });
	$("#application-BG").css({ "opacity": '0' });
	$("#orderDetails").css({ "background" : "transparent" });
	$("#__page7").css({ "background" : "transparent" });
	$("#__page7-cont").css({ "background" : "transparent" });
	$("#__page7-scroll").css({ "background" : "transparent" });
	$("#__page6").css({ "background" : "transparent" });
	$("#__page6-cont").css({ "background" : "transparent" });
	$("#__page6-scroll").css({ "background" : "transparent" });

	// initialise video object and append it to container div
	crazyArchitectWorld.showVideo(obj.data.d);

}

/*
 * sets background of the documents tab white
 */
function oTurnFilled() {
	$("#application").css({ "background" : "" });

}

/**
 * Converts date in format for showing in datepicker
 * @param oArray -- array with date substrings split by '/'
 * @returns {String} -- formated date string
 */
function convertDate(oArray) {
	for(var i = 0; i < oArray.length; i++) {
		if(oArray[i] == "") {
			continue;
		} else {
			var tmp = oArray[i].split("Date(");
			var oDate = new Date(parseInt(tmp[1].substring(0, tmp[1].length-1)));
			return oDate.getDate() + "." + (oDate.getMonth()+1) + "." + oDate.getFullYear();
		}
	}
}

/**
 * Converts time in format for showing in timepicker
 * @param time -- time string
 * @returns {String} -- formated time string
 */
function convertTime(time) {
	return time.substring(0, 2) + ":" + time.substring(3, time.length-1);
}

sap.ui.controller("program-resources.controller.orderDetails", {

	/*
	 * function for filling data into detail fields
	 */
	oFillForm: function(data) {

		crazyArchitectWorld.removeModel(obj.data.d);
		var position = getOrderObjectPosition(obj.selPath.split("/"));

		// fill detail page
		oThis.byId("orderObjectHeader").setTitle(data.results[position].Orderid);
		oThis.byId("orderTypeText").setText(data.results[position].OrderType);
//		oThis.byId("orderMaintTypeText").setText(data.Orders[position].MaintType);
		oThis.byId("orderDescText").setText(data.results[position].ShortText);
		oThis.byId("objectTplnrText").setText(data.results[position].Funcloc);
		oThis.byId("objectEqunrText").setText(data.results[position].Equipment);
		oThis.byId("deadlineStartText").setText(convertDate(data.results[position].StartDate.split("/")));
		oThis.byId("deadlineEndText").setText(convertDate(data.results[position].ProductionFinishDate.split("/")));
		oThis.byId("responText").setText(data.results[position].Respcctr);

		// completion detail page
		var operations = obj.data.d.Orders.results[position].Operas;
		oThis.byId("processNrText").setText(operations.results[position].Activity);
		oThis.byId("processShtxtText").setText(data.results[position].ShortText);
		oThis.byId("completionNrText").setText("");
		oThis.byId("staffNrText").setText(operations.results[position].PersonNo);
		oThis.byId("staffText").setText(data.results[position].MnWkCtr);
		oThis.byId("durationText").setText(operations.results[position].WorkActivity);
//		oThis.byId("completionText").setText(data.Orders[position].Ruetxt);
		oThis.byId("startDateText").setText(convertDate(operations.results[position].LateSchedStartDate.split("/")));
		oThis.byId("startTimeText").setText(convertTime(operations.results[position].LateSchedStartTime.substring(2, operations.results[position].LateSchedStartTime.length-3)));
		oThis.byId("endDateText").setText(convertDate(operations.results[position].LateSchedFinDate.split("/")));
		oThis.byId("endTimeText").setText(convertTime(operations.results[position].LateSchedFinTime.substring(2, operations.results[position].LateSchedFinTime.length-3)));

		// material detail page
		var oModel = new sap.ui.model.json.JSONModel();

		oModel.setData(data);
		var matTable = oThis.byId("materialTable").setModel(oModel);

		var listItems = new sap.m.ColumnListItem({
			cells : [
			    new sap.m.Text({text : "{Material}"}),
			    new sap.m.Text({text : "{desc}"}),
			    new sap.m.Text({text : "{Menge}"}),
			    new sap.m.Text({text : "{Einheit}"})
			]});

		// bind object path to listelements
//		var selPath = "/Orders/" + position + "/Material";
		matTable.bindItems("/results", listItems);
	},

	/*
	 * function for canceling order overview and for back-navigation
	 */
	oCancelOrder: function() {
		// navigate back to initial screen
		oSplitApp.backMaster();
		oSplitApp.backDetail();
		// set position back to tiles and set cam element initial
		oSplitApp.setMode(sap.m.SplitAppMode.HideMode);
		crazyArchitectWorld.removeModel(obj.data.d);
		sap.ui.getCore().byId("__page2-navButton").setVisible(true);
		$("#application").css({ "background" : "" });
	},

	oOrderCompletion: function() {
		// completion message
		sap.m.MessageToast.show("Order " + obj.data.d.Orders.results[0].Orderid + " completed", {
			duration : 3000,
			my : "center",
			at : "center"
		});

		// navigate back to initial screen
		oSplitApp.backMaster();
		oSplitApp.backDetail();
		// initialize model
		crazyArchitectWorld.removeModel(obj.data.d);
		oSplitApp.setMode(sap.m.SplitAppMode.HideMode);
		sap.ui.getCore().byId("__page2-navButton").setVisible(true);
		$("#application").css({ "background" : "" });
	},

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf mvc.MVC
*/
	onInit: function() {
		oThis = this;
        this.getView().addDelegate({
        	onAfterShow: function(evt) {
        		if( !obj.initialized ){
	        		sap.ui.getCore().byId("orderDetails--documentButton-icon").attachEvent("press", oDocButtonClicked);
	        		sap.ui.getCore().byId("orderDetails--materialButton-icon").attachEvent("press", oTurnFilled);
	        		sap.ui.getCore().byId("orderDetails--detailButton-icon").attachEvent("press", oTurnFilled);
	        		sap.ui.getCore().byId("orderDetails--completionButton-icon").attachEvent("press", oTurnFilled);
        		}
        		obj.initialized = true; // helper variable for showing if video is initialised
//        		$("#application").css({ "background" : "" });
        		// attach onPress event to Tab-controls
        		//oSplitApp.showMaster();
        	}
        });
	}
});