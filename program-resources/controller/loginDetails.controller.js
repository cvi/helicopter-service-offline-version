/**
 * function for getting informations about functional location
 * @param applicationContext -- informations about smp application session
 */
function read(applicationContext) {

	obj.applicationContext.applicationEndpointURL = "http://p761.coil.sap.com:50000/com.orianda.logon";

	// encrypt data
	var data = JSON.stringify(obj);
	var bytes = Crypto.charenc.Binary.stringToBytes(data);
	var base64 = Crypto.util.bytesToBase64(bytes);
	// store encrypted data in localstorage
	localStorage.setItem("object", base64);
    app.initialize();
}

/**
 * Successcallback for init function
 * @param result -- contents information about login status
 */
function logonSuccessCallback(result) {
	// If result is not null, we have successful login
	if (result) {
		// Set the application Context
		obj.applicationContext = result;
		// Read the Products from Gateway and display
		read(obj.applicationContext);
	}
}

/**
 * Errorcallback
 * @param e -- error information
 */
function errorCallback(e) {
	alert("An error occurred");
	alert(JSON.stringify(e));
}

sap.ui.controller("program-resources.controller.loginDetails", {

	/**
	 * function for initialising smp informations
	 * @param oEvent -- controller instance
	 */
	init: function(oEvent) {
		// obj.applicationContext.applicationEndpointURL = "http://p761.coil.sap.com:50000/com.orianda.logon";
		// encrypt data
		var data = JSON.stringify(obj);
		var bytes = Crypto.charenc.Binary.stringToBytes(data);
		var base64 = Crypto.util.bytesToBase64(bytes);
		// store encrypted data in localstorage
		localStorage.setItem("object", base64);
		app.initialize();
		/*
		//Application ID on SMP 3.0
		var appId = "com.orianda.logon";
		// Optional initial connection context
		// Sets defaults on MAF Login Component
		var context = {
			"serverHost" : "p761.coil.sap.com", //Hostname: VSSMP300, p761.coil.sap.com
			"serverPort" : "50000", //SMP 3.0 Server port
			"https" : "false", //Use HTTPS?
		};
		// Init the Logon component
		sap.Logon.init(logonSuccessCallback, errorCallback, appId, context, sap.logon.IabUi); */
	},

/**
* Called when a controller is instantiated and its View controls (if available) are already created.
* Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
* @memberOf mvc.MVC
*/
	onInit: function() {
		obj.applicationContext = null;
        this.getView().addDelegate({
        	onAfterShow: function(evt) {
        		// hide master button during login session
        		document.getElementById('application-MasterBtn').style.visibility = "hidden";
        	}
        });
	}
});