sap.ui.jsview("program-resources.views.loginDetails", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf mvc.MVC
	*/ 
	getControllerName : function() {
		return "program-resources.controller.loginDetails";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf mvc.MVC
	*/ 
	createContent : function(oController) {
 		// create login form
		var oLoginForm = new sap.m.FlexBox({
			items : [
			    new sap.m.Button("oLoginButton", {
			    	text : "Login",
			    	width : "23em",
			    	press : [oController.init, oController]
			    })			    
			],
			direction : "Column",
			height : "20em",
			alignItems : "Center"
		});
		
		return new sap.m.Page({
			title: "Login",
			content: oLoginForm
		});
	}
});