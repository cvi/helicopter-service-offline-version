sap.ui.jsview("program-resources.views.initDetails", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf mvc.MVC
	*/ 
	getControllerName : function() {
		return "program-resources.controller.initDetails";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf mvc.MVC
	*/ 
	createContent : function(oController) {		
		//Numeric Content
		new sap.suite.ui.commons.NumericContent(
				this.createId("numberOfNotifications"),
				{	size : sap.suite.ui.commons.InfoTileSize.L,
					icon : "sap-icon://inbox"
				}
		).placeAt("tile-div-1-1-number-Of-Notifications");

		new sap.suite.ui.commons.NumericContent(
				this.createId("numberOfOrders"),
				{	size : sap.suite.ui.commons.InfoTileSize.L,
					icon : "sap-icon://activity-items"
				}
		).placeAt("tile-div-1-1-number-Of-Orders");
		
		new sap.suite.ui.commons.NumericContent(
				this.createId("aircraftAtwe"),
				{	size : sap.suite.ui.commons.InfoTileSize.L,
					scale : "kg",
//					icon : "sap-icon://compare"
				}				
		).placeAt("tile-div-1-3-aircraft-weight");	
		
		new sap.suite.ui.commons.NumericContent(
				this.createId("aircraftCms2"),
				{	size : sap.suite.ui.commons.InfoTileSize.L,
					scale : "kg",
//					icon : "sap-icon://compare-2"
				}				
		).placeAt("tile-div-1-3-aircraft-cms2");
		
		new sap.suite.ui.commons.NumericContent(
				this.createId("aircraftCms3"),
				{	size : sap.suite.ui.commons.InfoTileSize.L,
					scale : "kg",
				}				
		).placeAt("tile-div-1-3-aircraft-cms3");	
		
		new sap.m.Label(				
				this.createId("heliApplication"),
				{	design : sap.m.LabelDesign.Standard ,
		}).placeAt("tile-div-1-2-heli-application");

		new sap.m.Label(				
				this.createId("planningInformation"),
				{	design : sap.m.LabelDesign.Standard ,
		}).placeAt("tile-div-2-2-plan");
		
		new sap.m.Label(				
				this.createId("objectInformation"),
				{	design : sap.m.LabelDesign.Standard ,
		}).placeAt("tile-div-2-2-object");
		
		new sap.m.Label(
				this.createId("immatriculationNo"),
				{	design : sap.m.LabelDesign.Bold ,
		}).placeAt("tile-div-2-2-immatriculation-no");	
				
		new sap.m.List(this.createId("installedOptions"), {	
			backgroundDesign: sap.m.BackgroundDesign.Transparent,
			noDataText: " ",
		}).placeAt("tile-div-2-3-installed-options");
		
		var oARContainer = new sap.m.FlexBox(this.createId("ARContainer"), {});
		
		/**
		 * TODO: insert charts
		 * implement charts with vizKit and load it into AR-View. 
		 * implement Vizcontainer for various charts
		 */
		// create column chart
		var oColumnChart = new sap.viz.ui5.Column("initDetails--AC_HOURS_COUNTER", {
			width : "100%",
			height : "100%",
			plotArea : {
			//'colorPalette' : d3.scale.category20().range()
			}
		});
		oColumnChart.getLegend().setVisible(false);
		oColumnChart.placeAt("tile-div-3-aircraft-hours");
		/** 		 		                                  
		 *                                  
		 *                    
		 **/
		var oColumnChart = new sap.viz.ui5.Column("initDetails--AC_CYCLES_COUNTER", {
			width : "100%",
			height : "100%",
			plotArea : {
			//'colorPalette' : d3.scale.category20().range()
			}
		});
		oColumnChart.getLegend().setVisible(false);
		oColumnChart.placeAt("tile-div-3-aircraft-cycles");
		/**		 		                                  
		 *                                  
		 *                    
		 **/
		
 		// get equip controller instance
 		var equipCont = sap.ui.getCore().byId("equipMaster").getController();
 		
		// create button and hide it
		var createButton = new sap.m.Button("createButton", {
			icon : "sap-icon://add",
			visible : {
				path : "local>/inEdit",
				formatter : function(inEdit) { return !inEdit; }
			},
			press : [ equipCont.createButtonPress, equipCont ]
		});
		
		// create busy indicator
		var busyIndicator = new sap.m.BusyIndicator('flBusy', {
			visible: false,
			customIconWidth: '4em',
			customIconHeight: '4em',
			CustomIconRotationSpeed: 1500
		});
		
		// snap button for freezing picture and AR-Data
		var snapCancel = new sap.m.Button(this.createId("snapCancel"), {
			icon : "sap-icon://delete",
			visible : {
				path : "local>/inEdit",
				formatter : function(inEdit) { return !inEdit; }
			},
			press : [ oController.cancelSnapShot, oController ]
		});	
		
		// page footer
		var pageFooter = new sap.m.Bar({
			contentRight: snapCancel
		});
	
		var page = new sap.m.Page(this.createId("initPage"), {
			title: "Track a Functional Location...",
			enableScrolling: false,
			content: [oARContainer, busyIndicator],
			footer: pageFooter
		});
		
		// hide create, details and snapCancel button
		createButton.setVisible(false);
		snapCancel.setVisible(false);
		sap.ui.getCore().byId('application-MasterBtn').setVisible(false);
		
		// include addbutton in the view
		page.addHeaderContent(createButton);
 		return page;
	}

});