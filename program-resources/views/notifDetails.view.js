sap.ui.jsview("program-resources.views.notifDetails", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf mvc.MVC
	*/ 
	getControllerName : function() {
		return "program-resources.controller.notifDetails";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf mvc.MVC
	*/ 
	createContent : function(oController) {
		
		// get Icon Names
		jQuery.sap.require("sap.ui.core.IconPool");
		var aNames = sap.ui.core.IconPool.getIconNames();
		
		// Funcloc row
		var funcloc = new sap.m.FlexBox({
			items : [ 
        	    new sap.m.Label({
        	    	text : "Functional Location Nr.:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
        	    }),
        	    new sap.m.Input(this.createId("notifFunclocField"), {
        	    	editable : false,
			    	width : "25em"
        	    })
        	]
		});
		
		// Equipment Nr. row
		var equnr = new sap.m.FlexBox({
			items : [ 
	    	    new sap.m.Label({
	    	    	text : "Equipment Nr.:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
	    	    }),
	    	    new sap.m.Input(this.createId("notifEquipField"), {
	    	    	editable : false,
			    	width : "25em"
	    	    })
	    	]
		});
		
		// Equipment row
		var equipment = new sap.m.FlexBox({
			items : [ 
	    	    new sap.m.Label({
	    	    	text : "Equipment",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
	    	    }),
	    	    new sap.m.Input(this.createId("notifEquipName"), {
	    	    	editable : false,
			    	width : "25em"
	    	    }),   
	    	]
		});
		
		// Shorttext
		var shortText = new sap.m.FlexBox({
			items : [
        	    new sap.m.Label({
        	    	text : "Shorttext:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
        	    }),
        	    new sap.m.Input(this.createId("notifShtxtField"), {
        	    	editable : false,
        	    	width : "25em"
        	    })
    	    ]
		});
		
		// Date and Time
		var dateTime = new sap.m.FlexBox({
			items : [ 
        	    new sap.m.Label({
        	    	text : "Date and Time:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
        	    }),
        	    new sap.m.DateTimeInput(this.createId("notifDatePicker"), {
        	    	type : sap.m.DateTimeInputType.Date,
        	    	editable : false,
        	    	width : "13em"
        	    }),
        	    new sap.m.DateTimeInput(this.createId("notifTimePicker"), {
        	    	type : sap.m.DateTimeInputType.Time,
        	    	editable : false,
        	    	width : "12em"
        	    })
            ]
		});
		
		// case description
		var longText = new sap.m.FlexBox({
			items : [
	    	    new sap.m.Label({
			    	text : "Case Description:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Start})
	    	    }),
	    	    new sap.m.TextArea(this.createId("notifDmgField"), {
	    			width : "25em",
	    			editable : false,
	    			rows : 8,
	    			cols: 132
	    		})
	    	]
		});
		
//		// pictures
//		var oImage1 = new sap.m.Button(this.createId("imageNotif1"), {
//			width: "10em",
//			styled: true,
//			style: "Default",
//			lite: true,
//			icon: sap.ui.core.IconPool.getIconURI(aNames[69]),
//			height: "100px",
//			enabled: false,
//			press: function(oEvent) {
//				handleImageClick(this);
//			}
//		});
//		
//		var oImage2 = new sap.m.Button(this.createId("imageNotif2"), {
//			width: "10em",
//			styled: true,
//			style: "Default",
//			lite: true,
//			icon: sap.ui.core.IconPool.getIconURI(aNames[69]),
//			height: "100px",
//			enabled: false,
//			press: function(oEvent) {
//				handleImageClick(this);
//			}
//		});
//		
//		var oImage3 = new sap.m.Button(this.createId("imageNotif3"), {
//			width: "10em", 
//			styled: true,
//			style: "Default",
//			lite: true,
//			icon: sap.ui.core.IconPool.getIconURI(aNames[69]),
//			height: "100px",
//			enabled: false,
//			press: function(oEvent) {
//				handleImageClick(this);
//			}
//		});
		
		// Picturebox
//		var picBox = new sap.m.FlexBox({
//			items : [ 
//			    new sap.m.Label({
//			    	text : "Pictures:",
//					width : "15em",
//					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Start})
//			    }), 
//			    oImage1, 
//			    oImage2, 
//			    oImage3]
//		});
		
		// Priority
		var prioBox = new sap.m.FlexBox({
			items : [ 
			    new sap.m.Label({
        	    	text : "Priority:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({
						alignSelf: sap.m.FlexAlignSelf.Center
					})
			    }),
        	    new sap.m.Select(this.createId("notifPrioField"), {
			    	width : "25em",
			    	editable : false,
			    	items : [
			    		new sap.ui.core.Item({					    
				    		text: "none",
				    		key: ""
			    		}),
			    		new sap.ui.core.Item({					    
				    		text: "low",
				    		key: "4"
			    		}),
			    		new sap.ui.core.Item({
			    			text: "average",
			    			key: "3",
			    		}),
			    		new sap.ui.core.Item({					    
				    		text: "high",
				    		key: "2"
			    		}),
			    		new sap.ui.core.Item({
			    			text: "very high",
			    			key: "1",
			    		}),
			    	]
        	    })
		    ]
		});
		
		
		// create button for page footer
		var cancelButton = new sap.m.Button(this.createId("overviewCancelButton"), {
			icon : "sap-icon://decline",
			visible : {
				path : "local>/inEdit",
				formatter : function(inEdit) { return !inEdit; }
			},
			press : oController.oCancelNotifOverview
		});
		
		// page footer
		var pageFooter = new sap.m.Bar({
			contentRight : [cancelButton]
		});

		// set elements in box for alignment
		var content = new sap.m.FlexBox({
			items : [
			    funcloc, equnr, equipment, shortText, dateTime, longText, prioBox //, picBox
			],
			direction : "Column",
			alignItems : sap.m.FlexAlignItems.Center 
		});
		
		
		// Include all flexboxes into page element
		return new sap.m.Page({
			title: "Notification Details",
			showNavButton : false,
			content: content,
			footer : pageFooter
		});	
	}
});