sap.ui.jsview("program-resources.views.equipDetails", {

	/** Specifies the Controller belonging to this View. 
	* In the case that it is not implemented, or that "null" is returned, this View does not have a Controller.
	* @memberOf mvc.MVC
	*/ 
	getControllerName : function() {
		return "program-resources.controller.equipDetails";
	},

	/** Is initially called once after the Controller has been instantiated. It is the place where the UI is constructed. 
	* Since the Controller is given to this method, its event handlers can be attached right away. 
	* @memberOf mvc.MVC
	*/ 
	createContent : function(oController) {
		
		// get Icon Names
		jQuery.sap.require("sap.ui.core.IconPool");
		var aNames = sap.ui.core.IconPool.getIconNames();
		
		// Funcloc row
		var funcloc = new sap.m.FlexBox({
			items : [ 
        	    new sap.m.Label({
        	    	text : "Functional Location Nr.:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
        	    }),
        	    new sap.m.Input(this.createId("funclocField"), {
        	    	editable : false,
			    	width : "25em"
        	    })
        	]
		});
		
		// Equipment Nr. row
		var equnr = new sap.m.FlexBox({
			items : [ 
	    	    new sap.m.Label({
	    	    	text : "Equipment Nr.:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
	    	    }),
	    	    new sap.m.Input(this.createId("equipField"), {
	    	    	editable : false,
			    	width : "25em"
	    	    })
	    	]
		});
		
		// Equipment row
		var equipment = new sap.m.FlexBox({
			items : [ 
	    	    new sap.m.Label({
	    	    	text : "Equipment",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
	    	    }),
	    	    new sap.m.Input(this.createId("equipName"), {
	    	    	editable : false,
			    	width : "25em"
	    	    }),   
	    	]
		});
		
		// Shorttext
		var shortText = new sap.m.FlexBox({
			items : [
        	    new sap.m.Label({
        	    	text : "Shorttext:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
        	    }),
        	    new sap.m.Input(this.createId("shtxtField"), {
			    	width : "25em"
        	    })
    	    ]
		});
		
		// Date and Time
		var dateTime = new sap.m.FlexBox({
			items : [ 
        	    new sap.m.Label({
        	    	text : "Date and Time:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Center})
        	    }),
        	    new sap.m.DateTimeInput(this.createId("datePicker"), {
        	    	type : sap.m.DateTimeInputType.Date,
        	    	width : "13em"
        	    }),
        	    new sap.m.DateTimeInput(this.createId("timePicker"), {
        	    	type : sap.m.DateTimeInputType.Time,
        	    	width : "12em"
        	    })
            ]
		});
		
		// case description
		var longText = new sap.m.FlexBox({
			items : [
	    	    new sap.m.Label({
			    	text : "Case Description:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({alignSelf: sap.m.FlexAlignSelf.Start})
	    	    }),
	    	    new sap.m.TextArea(this.createId("dmgField"), {
	    			width : "25em",
	    			rows : 8,
	    			cols: 132
	    		})
	    	]
		});
		
		// Priority
		var prioBox = new sap.m.FlexBox({
			items : [ 
			    new sap.m.Label({
        	    	text : "Priority:",
					width : "15em",
					layoutData : new sap.m.FlexItemData({
						alignSelf: sap.m.FlexAlignSelf.Center
					})
			    }),
        	    new sap.m.Select("prioField", {
			    	width : "25em",
			    	items : [
			    		new sap.ui.core.Item({					    
				    		text: "none",
				    		key: ""
			    		}),
			    		new sap.ui.core.Item({					    
				    		text: "low",
				    		key: "4"
			    		}),
			    		new sap.ui.core.Item({
			    			text: "average",
			    			key: "3",
			    		}),
			    		new sap.ui.core.Item({					    
				    		text: "high",
				    		key: "2"
			    		}),
			    		new sap.ui.core.Item({
			    			text: "very high",
			    			key: "1",
			    		}),
			    	]
        	    })
		    ]
		});
		
		// create button for page footer
		var saveButton = new sap.m.Button(this.createId("notifSaveButton"), {
			icon : "sap-icon://save",
			visible : {
				path : "local>/inEdit",
				formatter : function(inEdit) { return !inEdit; }
			},
			press : [oController.oSaveNotification, oController]
		});
		
		// create button for page footer
		var cancelButton = new sap.m.Button(this.createId("notifCancelButton"), {
			icon : "sap-icon://decline",
			visible : {
				path : "local>/inEdit",
				formatter : function(inEdit) { return !inEdit; }
			},
			press : oController.oCancelNotification
		});
		
		// create busy indicator
		var busyIndicator = new sap.m.BusyIndicator('eqBusy', {
			visible: false,
			customIconWidth: '4em',
			customIconHeight: '4em',
			CustomIconRotationSpeed: 1500
		});
		
		// page footer
		var pageFooter = new sap.m.Bar({
			contentRight : [cancelButton, saveButton],	
		});

		// set elements in box for alignment
		var content = new sap.m.FlexBox({
			items : [
			    funcloc, equnr, equipment, shortText, dateTime, longText, prioBox, busyIndicator
			],
			direction : "Column",
			alignItems : sap.m.FlexAlignItems.Center 
		});
		
		
		// Include all flexboxes into page element
		return new sap.m.Page({
			title: "New Notification",
			showNavButton : false,
			content: content,
			footer : pageFooter
		});	
	}
});