/*
 * Licensed to the Apache Software Foundation (ASF) under one
 * or more contributor license agreements.  See the NOTICE file
 * distributed with this work for additional information
 * regarding copyright ownership.  The ASF licenses this file
 * to you under the Apache License, Version 2.0 (the
 * "License"); you may not use this file except in compliance
 * with the License.  You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing,
 * software distributed under the License is distributed on an
 * "AS IS" BASIS, WITHOUT WARRANTIES OR CONDITIONS OF ANY
 * KIND, either express or implied.  See the License for the
 * specific language governing permissions and limitations
 * under the License.
 */
var app = {
    // Application Constructor
    initialize: function() {
    	console.log('jump into app.init');
        this.bindEvents();
    },
    // Bind Event Listeners
    //
    // Bind any events that are required on startup. Common events are:
    // 'load', 'deviceready', 'offline', and 'online'.
    bindEvents: function() {
    		document.addEventListener('deviceready', this.onDeviceReady, false);
    },
    // deviceready Event Handler
    //
    // The scope of 'this' is the event. In order to call the 'receivedEvent'
    // function, we must explicity call 'app.receivedEvent(...);'
    onDeviceReady: function() {
    	console.log('jump into onDeviceReady');
        app.receivedEvent('deviceready');

        // Default handlers
        var successCallback = function() {
            // do something
        	console.log('successCallback');
        };
        var errorCallback = function(errMsg) {
            alert("Error! " + errMsg);
        }
        
        var oriWorld = new Object();
        oriWorld.path = "www/world.html";
        oriWorld.requiredFeatures = new Array("2d_tracking");
        oriWorld.startupConfiguration = new Object();
        oriWorld.startupConfiguration.camera_position = "back";
        
        app.loadARchitectWorld(oriWorld);
        
//         isDeviceSupported to check the device has the required hardware and software.
//        WikitudePlugin.isDeviceSupported(function() {
//        	console.log('jump into WikitudePlugin.isDeviceSupported()');
//          // set callback for VE Viewer call -- set parameters
//          if(cordova.platformId != 'android') {
//        	  WikitudePlugin.setOnUrlInvokeCallback(function(){ window.open("veviewer://") });
//          } else {
//        	  WikitudePlugin.setOnUrlInvokeCallback(function(){
//        		  // check if Application is installed
//        		  navigator.startApp.check("com.sap.ve", function(message) { /* success */
//            		  // if Application is installed call it
//        			  navigator.startApp.start("com.sap.ve", function(message) { /* success */
//            		      console.log(message); // => OK
//            		  },
//            		  function(error) { /* error */
//            		      console.log('47', error);
//            		  })
//    			  },
//    			  // else notificate User
//    			  function(error) { /* error */
//    				  sap.m.MessageToast.show("Called Application is not installed", {
//    				   	  duration : 5000,
//    					  my : "center",
//    					  at : "center"
//    				  });
//    				  window.open("https://play.google.com/store/apps/details?id=com.sap.ve&hl=de", "_system" );
//    			      console.log('47', error);
//    			  });
//        	  });
//
//          }
//          // Success callback - load the ARchitectWorld with the path to your world's HTML file
//          
//          
//        },
//
//        function() {
//          // Error callback - the device isn't supported
//          navigator.notification.alert('Unable to launch the AR world on this device!');
//        });
    },
	loadARchitectWorld : function(genericWorld) {
        // check if the current device is able to launch ARchitect Worlds
        WikitudePlugin.isDeviceSupported(function() {
        	console.log('register Callback');
            WikitudePlugin.setOnUrlInvokeCallback(app.onURLInvoked);

            WikitudePlugin.loadARchitectWorld(function successFn(loadedURL) {
            	console.log('success calling wold');
                /* Respond to successful world loading if you need to */ 
            }, function errorFn(error) {
                alert('Loading AR web view failed: ' + error);
            },
            genericWorld.path, genericWorld.requiredFeatures, genericWorld.startupConfiguration
            );
        }, function(errorMessage) {
            alert(errorMessage);
        },
        genericWorld.requiredFeatures
        );
    },
    onURLInvoked: function(url) {
    	console.log('URL invoke' + url);
        if ('closeWikitudePlugin' == url.substring(22) ) {
            WikitudePlugin.close();
        }
        else if ('action' == url.substring(22) ) {
        	console.log('Enter action method');
        	 if(cordova.platformId != 'android') {
            	   window.open("veviewer://");
              } else {
            	  console.log(' check com.sap.ve installed?');
         		  // check if Application is installed
         		  navigator.startApp.check("com.sap.ve", function(message) { /* success */
             		  // if Application is installed call it
         			  console.log(' com.sap.ve installed');
         			  navigator.startApp.start("com.sap.ve", function(message) { /* success */
             		      console.log(message); // => OK
             		  },
             		  function(error) { /* error */
             		      console.log('47', error);
             		  })
     			  },
     			  // else notificate User
     			  function(error) { /* error */
     				  sap.m.MessageToast.show("Called Application is not installed", {
     				   	  duration : 5000,
     					  my : "center",
     					  at : "center"
     				  });
     				  window.open("https://play.google.com/store/apps/details?id=com.sap.ve&hl=de", "_system" );
     			      console.log('47', error);
     			  });
              }
        	
//        	 if(cordova.platformId != 'android') {
//           	  WikitudePlugin.setOnUrlInvokeCallback(function(){ window.open("veviewer://") });
//             } else {
//            	
//           	  WikitudePlugin.setOnUrlInvokeCallback(function(){
//           		console.log(' check com.sap.ve installed?');
//           		  // check if Application is installed
//           		  navigator.startApp.check("com.sap.ve", function(message) { /* success */
//               		  // if Application is installed call it
//           			  console.log(' com.sap.ve installed');
//           			  navigator.startApp.start("com.sap.ve", function(message) { /* success */
//               		      console.log(message); // => OK
//               		  },
//               		  function(error) { /* error */
//               		      console.log('47', error);
//               		  })
//       			  },
//       			  // else notificate User
//       			  function(error) { /* error */
//       				  sap.m.MessageToast.show("Called Application is not installed", {
//       				   	  duration : 5000,
//       					  my : "center",
//       					  at : "center"
//       				  });
//       				  window.open("https://play.google.com/store/apps/details?id=com.sap.ve&hl=de", "_system" );
//       			      console.log('47', error);
//       			  });
//           	  });
//
//             }
        //Error Case no matching URL found
        }else {
            alert('ARchitect => PhoneGap ' + url);
        }
    },
    // Update DOM on a Received Event
    receivedEvent: function(id) {
        console.log('Received Event: ' + id);
    }
};
